<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSnippetsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('snippets', function (Blueprint $table) {
            $table->id();

            $table->bigInteger('owner')->unsigned();
            $table->foreign('owner')->references('id')->on('users');

            $table->string('title');
            $table->text('code');

            $trueEnum =
            [
              'true',
              'false'
            ];
            $table->enum('linenos', $trueEnum);

            $languages =
            [
              'Javascript',
              'CPP',
              'Python',
              'HTML',
              'PHP'
            ];
            $table->enum('language', $languages);

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('snippets');
    }
}

<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\User;
use App\Snippets;


class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        // $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
      session_start();
      $userId = $_SESSION['token'];

      if (isset($_SESSION['token']))
      {
        $userId = $_SESSION['token'];
        $user = User::find($userId);

        $snippets = $this->GetUserSnippets($user);

        $data =
        [
          'user' => $user,
          'snippets' => $snippets
        ];
        return view('home', $data);
      }

      return redirect('/');
    }

    private function GetUserSnippets($user)
    {
      $snippets = Snippets::where('owner', $user->id)->paginate(4);
      return $snippets;
    }
}

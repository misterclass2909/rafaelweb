<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;

class LoginController extends Controller
{
    public function login(Request $request)
    {
      $authUserName = $request->input('name');
      $authUserPwd = $request->input('password');
      $user = User::where('username', $authUserName)->first();

      if ($user && $user->password == $authUserPwd)
      {
        $this->SaveUserName($user);
        return redirect()->route('home');
      }

      //Redirect if no login
      return redirect('/');
    }

    private function SaveUserName($user)
    {
      session_start();
      $_SESSION['token'] = $user->id;
    }
}

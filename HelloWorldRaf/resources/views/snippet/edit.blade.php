@extends('layouts.layout')

@section('content')
  <div class="limitation">
    <form class="container-form" id = "snippet-form" action="{{ route('snippets.update', $snippet->id) }}" method="post">

      <div id = "task-basis-inputs" class="form-inputs flex-row-between">
        <input type="text" name="name" placeholder = "Название проекта" autocomplete="off"
         value = "{{ $snippet->title }}">
        <input type="text" name="description" placeholder = "Описание" autocomplete="off"
         value = "{{ $snippet->description }}">
         @php
          $languages = array('javascript', 'cpp', 'python', 'html', 'php');
          $results = array('done', 'unresolved');
         @endphp
        <select class="prog-lang" name="prog-lang">
          @foreach ($languages as $value)
            @if ($value == $snippet->language)
              <option value="{{ $value }}" selected>{{ ucfirst($value) }}</option>
            @else
              <option value="{{ $value }}">{{ ucfirst($value) }}</option>
            @endif
          @endforeach
          <!-- <option value="javascript">Javascript</option>
          <option value="cpp">C++</option>
          <option value="python">Python</option>
          <option value="html">HTML</option>
          <option value="php">PHP</option> -->
        </select>
        <select class="select-status" name="select-status">
          @foreach ($results as $value)
            @if ($value == $snippet->linenos)
              <option value="{{ $value }}" selected>{{ ucfirst($value) }}</option>
            @else
              <option value="{{ $value }}">{{ ucfirst($value) }}</option>
            @endif
          @endforeach
          <!-- <option value="done">Завершена</option>
          <option value="unresolved">Не завершена</option> -->
        </select>
      </div>
      <div class="form-inputs textarea">
        <textarea name="code" placeholder="Вставьте сюда код...">"{{ $snippet->code }}"</textarea>
      </div>
      <div class="form-buttons flex-row-end" id = "snippet-buttons">
        <input type="reset" value="Отмена" id = "task-reset">
        <input type="submit" value="Сохранить" class = "btn-blue" id = "task-submit">
      </div>
      @csrf
    </form>
  </div>
@endsection

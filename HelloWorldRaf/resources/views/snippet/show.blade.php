@extends('layouts.layout')

@section('content')
  <div class="limitation">
    <div id = "current-task">
      <div class="task">
        <div class="task-lang"></div>
        <div class="task-content">
          <h3>{{ $snippet->title }}</h3>
          <p>
          {{ $snippet->description }}
          </p>
          @if ($snippet->linenos == 'done')
            <div class = "task-status task-done">
              <!-- Css after content -->
            </div>
          @else
            <div class = "task-status task-unresolved">
              <!-- Css after content -->
            </div>
          @endif
        </div>
      </div>
      <div class="flex-between">
        <a href="{{ route('snippets.edit', $snippet->id) }}">Редактировать</a>
        <a href="{{ route('snippets.delete', $snippet->id) }}">Удалить</a>
      </div>
    </div>
  </div>
@endsection

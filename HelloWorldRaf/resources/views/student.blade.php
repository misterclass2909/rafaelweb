@extends('layouts.layout')

@section('content')
  <div class="limitation">
    <div id = "student">
      <div id = "student-information">
        <div id = "student-header">
          <a href="#" class = "back"></a>
          <div class="user-avatar"></div>
        </div>
        <div id="student-content">
          <p>{{ $currentUser->username }}</p>
          <p>@somebody</p>
          <p>
            {{ $currentUser->description }}
          </p>
        </div>
        <div class="flex-column-end">
          <a href="{{ route('students') }}">Вернуться к списку</a>
        </div>
      </div>
      <div class="slider">
        <div class="slider-content">
          <a href="{{$currentUser->snippets->previousPageUrl()}}">
            <div class="slider-arrow"></div>
          </a>
          <div class="tasks-list">
            @foreach($currentUser->snippets as $snippet)
            <div class="task">
              <p>{{ $snippet->title }}</p>
              <a href="{{ route('snippets.show', $snippet->id) }}">
                <div class="task-lang"></div>
              </a>
              <div class="task-content">
                <p>
                  {{ $snippet->description }}
                </p>
                @if ($snippet->linenos == 'done')
                  <div class = "task-status task-done">
                    <!-- Css after content -->
                  </div>
                @else
                  <div class = "task-status task-unresolved">
                    <!-- Css after content -->
                  </div>
                @endif
              </div>
            </div>
            @endforeach
          </div>
          <a href="{{$currentUser->snippets->nextPageUrl()}}">
            <div class="slider-arrow"></div>
          </a>
        </div>
        <div class="slider-paginator">
          {{ $currentUser->snippets->links() }}
        </div>
      </div>
    </div>
  </div>
@endsection

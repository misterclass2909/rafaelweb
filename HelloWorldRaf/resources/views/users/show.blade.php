@extends('layouts.layout')

@section('content')
  <div class="limitation">
    <div class="slider">
      <div class="slider-content">
        <div class="slider-arrow"></div>
        <div class="tasks-list">
          <div class="task">
            <div class="task-lang"></div>
            <div class="task-content">
              <h3>Калькулятор</h3>
              <div class="user-name">
                <p>{{ $username }}</p>
                <p>@somebody</p>
              </div>
              <p>
                Lorem ipsum dolor sit amet, consectetur adipisicing elit,
                sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.
                Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris
                nisi ut aliquip ex ea commodo consequat.
              </p>
              <div class = "task-status task-done">
                <!-- Css after content -->
              </div>
            </div>
          </div>
        </div>
        <div class="slider-arrow"></div>
      </div>
      <div class="slider-paginator">
        <div class="slide slide-active"></div>
        <div class="slide"></div>
        <div class="slide"></div>
        <div class="slide"></div>
      </div>
    </div>
  </div>
@endsection

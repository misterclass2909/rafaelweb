<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>Hello World</title>
        <link rel="stylesheet" href="{{ asset('css/style.css') }}">

    </head>
    <body>
      <div id="loginBG">
        <header>
          <div class="limitation">
            <a href="#" id = "header-logo">
              <p>Hello World</p>
            </a>
          </div>
        </header>

        <main>
          <div class="limitation flex-vertical-center">
            <form class="container-form" id = "login-form"
                  action="{{ route('login') }}" method="post">
                  @csrf
              <h2>Привет!</h2>
              <h2>У нас появились новые задачи!</h2>
              <p>Быстрее заходи и выполняй их</p>
              <div class="form-inputs">
                <input type="text" name="name" placeholder = "Логин">
                <input type="password" name="password" placeholder = "Пароль">
              </div>
              <div class="form-buttons">
                <input type="submit" value="Авторизоваться" class = "btn-blue">
              </div>
            </form>
          </div>
        </main>
      </div>
    </body>
</html>

@extends('layouts.layout')

@section('content')
  <div class="limitation">
    <div class="slider">
      <div class="slider-content">
        <a href="{{$snippets->previousPageUrl()}}">
          <div class="slider-arrow"></div>
        </a>
        <div class="tasks-list">
          @foreach($snippets as $snippet)
            <div class="task">
              <a href="{{ route('snippets.show', $snippet->id) }}">
                <div class="task-lang"></div>
              </a>
              <div class="task-content">
                <h3>{{ $snippet->title }}</h3>
                <div class="user-name">
                  <p>{{ $user->username }}</p>
                  <p>@somebody</p>
                </div>
                <p>
                {{ $snippet->description }}
                </p>
                @if ($snippet->linenos == 'done')
                  <div class = "task-status task-done">
                    <!-- Css after content -->
                  </div>
                @else
                  <div class = "task-status task-unresolved">
                    <!-- Css after content -->
                  </div>
                @endif
              </div>
            </div>
          @endforeach
        </div>
        <a href="{{$snippets->nextPageUrl()}}">
          <div class="slider-arrow"></div>
        </a>
      </div>
      <div class="slider-paginator">
        {{ $snippets->links() }}
      </div>
    </div>
  </div>
@endsection

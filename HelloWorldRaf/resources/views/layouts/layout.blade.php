<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>Hello World</title>
        <link rel="stylesheet" href="{{ asset('css/style.css') }}">

        <!-- Jquery CDN -->
        <script
          src="https://code.jquery.com/jquery-3.5.1.js"
          integrity="sha256-QWo7LDvxbWT2tbbQ97B53yJnYU3WhH/C8ycbRAkjPDc="
          crossorigin="anonymous"></script>

        <script src="{{ asset('js/script.js') }}" charset="utf-8"></script>

    </head>
    <body>
      <header>
        <div class="limitation">
          <div class="mobile-menu">
            <div class="mobile-menu-bg"></div>
            <ul class = "mobile-nav">
              <li>
                <a href="{{ route('home') }}" id = "list-icon"></a>
              </li>
              <li>
                <a href="{{ route('snippets.create') }}" id = "add-icon"></a>
              </li>
              <li>
                <a href="{{ route('students') }}" id = "group-icon"></a>
              </li>
              <li>
                <a href="{{ route('index') }}" id = "logout-icon">
                </a>
              </li>
            </ul>
          </div>
          <div class="empty">
            <!-- Used for correct header justify content if menu is active -->
          </div>

          <div id="header-content">
            <a href="#" id = "header-logo">
              <p>Hello World</p>
            </a>
            <p>Программисты рождаются здесь</p>
          </div>

          <div id="header-avatar">
            <div id = "header-avatar-content">
              <p>{{ $user->username }}</p>
              <p>@somebody</p>
            </div>
            <div class="user-avatar"></div>
          </div>
        </div>
      </header>

      <main>
        @yield('content')
      </main>

      <footer>
        <div class="limitation">
          <p>Made by rafik_kryt</p>
        </div>
      </footer>
    </body>
</html>

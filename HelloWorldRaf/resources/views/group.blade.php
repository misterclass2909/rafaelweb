@extends('layouts.layout')

@section('content')
  <div class="limitation">
    <div class="slider">
      <div class="slider-content">
        <a href="{{$users->previousPageUrl()}}">
          <div class="slider-arrow"></div>
        </a>
        <div id="students-list">
          @foreach($users as $el)
            <div class="student-card">
              <a href="{{ route('users', $el->id) }}">
                <div class="user-avatar"></div>
              </a>
              <div class="student-content">
                <div class="user-name">
                  <p>{{ $el->username }}</p>
                  <p>@somebody</p>
                </div>
                <div class="student-tasks">
                  @foreach($el->snippets as $snippet)
                  <a href="{{ route('snippets.show', $snippet->id) }}">
                    <div class="task">
                      <p>{{ $snippet->title }}</p>
                      <div class="task-lang"></div>
                    </div>
                  </a>
                  @endforeach
                </div>
              </div>
            </div>
          @endforeach
        </div>
        <a href="{{$users->nextPageUrl()}}">
          <div class="slider-arrow"></div>
        </a>
      </div>
      <div class="slider-paginator">
        {{ $users->links() }}
      </div>
    </div>
  </div>
@endsection
